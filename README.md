# TD CICD : GitLab CI + SonarQube + PHPUnit + Docker


## Etape 1 : Création de l'environnement

### Préparation de la machine virtuelle

Créer une VM avec les caractèristiques suivantes : 
   - hostname : $group-cicd01
   - disque : 40 Go
   - ram : 8Go
   - cpu : 2 socket, 2 cores
   - OS : [Ubuntu Server 22.04 LTS](https://releases.ubuntu.com/22.04.3/ubuntu-22.04.3-live-server-amd64.iso)


### Installer les paquets supplémentaires

Configurer vos proxy pour aller récupérer les paquets apt sur les dépots internet.
Editer le fichier /etc/apt/apt.conf.d/proxy.conf :
  ```
  Acquire::http::Proxy "http://<user>:<password>@<proxyserver>:<proxyport>/";
  Acquire::https::Proxy "http://<user>:<password>@<proxyserver>:<proxyport>/";
  ```


Vérifier que vous pouvez bien installer vos paquets : 
```
# apt -y update
# apt -y upgrade
```

Installer les packages docker : 
```
# apt -y install docker.io 
```

Configurer les proxy pour dockerd en créant le fichier /etc/systemd/system/docker.service.d/http-proxy.conf :
```
[Service]
Environment="HTTP_PROXY=http://<user>:<password>@<proxyserver>:<proxyport>/"
Environment="HTTPS_PROXY=http://<user>:<password>@<proxyserver>:<proxyport>/"
```

### Lancer SonarQube et vérifier qu'il est opérationnel 

Nous allons utiliser l'image docker sonarqube:lts disponible sur [DockerHub](https://hub.docker.com/_/sonarqube/)

```
docker run -d --name sonarqube \
    -e SONAR_ES_BOOTSTRAP_CHECKS_DISABLE=true \
    -p 9000:9000 \
    sonarqube:lts
```

Vérifier que vous accèder bien à l'interface de SonarQube à l'URL suivante ou $ip est l'IP de votre VM $group-cicd01 :
http://$ip:9000/

### Lancer GitLab-runner et vérifier qu'il est opérationnel 

Nous allons utiliser l'image docker gitlab-runner:latest disponible sur [DockerHub](https://hub.docker.com/r/gitlab/gitlab-runner)

```
# mkdir -p /srv/gitlab-runner/config
# docker run -d --name gitlab-runner \ 
    --restart always \
    -v /srv/gitlab-runner/config:/etc/gitlab-runner  \
    -v /var/run/docker.sock:/var/run/docker.sock \
    gitlab/gitlab-runner:latest
```

### Validation étape 1

A faire valider : 

- [ ] Machine virtuelle OK
- [ ] Container docker gitlab-runner & sonarqube OK 
- [ ] Accès à l'interface SonarQube OK


## Etape 2 : Configuration du projet GitLab & SonarQube

### Créer un compte sur gilab.com 

Créer votre compte [Gitlab.com](https://gitlab.com/users/sign_in)

### Créer un projet sur gitlab.com

Créer un projet dans votre [Espace Gitlab](https://gitlab.com/).

Nom du projet : but3-TD-$groupe
Projet en private
Ajout utilisateurs : 
   -  @alex.linte
   -  @Xutig


### Enregistrer le gitlab-runner sur votre projet

Désactiver les "shared runner" pour votre projet.
Créer un runner dédié pour votre projet avec les propriétés suivantes : 
  - tag : but-$groupe
  - description : but-runner-$groupe
  - Locked for current projet
Récupérer le RUNNER_TOKEN associé 

```
# docker run --rm -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --token "<RUNNER_TOKEN>" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "docker-runner"
```

Vérifier que votre runner est bien activé sur votre projet (point vert dans l'interface).

### Configurer SonarQube sur votre projet

Créer un [Personnal AccessToken GitLab](https://gitlab.com/-/user_settings/personal_access_tokens) avec les caractèristiques suivantes : 
 - nom : sonar
 - scopes : api, read_api, read_user, read_repository, read_registry


Se connecter sur l'interface SonarQube.
Créer un projet basé sur gitlab avec les caractèristiques suivantes :
  - nom : php-$groupe

Renseigner votre Personnal Access Token qui vous permettra de lié votre projet SonarQube avec votre projet gitlab

Suivre le tutoriel qui devrait vous amener à : 
  - Créer des variables d'environnement dans votre projet : 
    -   SONAR_HOST_URL qui contient l'URL de votre SonarQube
    -   SONAR_TOKEN qui contient un token d'accès à votre SonarQube 
  - Créer 2 fichiers dans le dépot de votre projet: 
    -   sonar-project.properties
    -   gitlab-ci.yaml

### Execution de la pipeline Gitlab

Lors de la modification du fichier gitlab-ci.yaml cela lance automatiquement la pipeline GitLab de votre projet.

### Validation étape 2

A faire valider : 

- [ ] Projet gitlab OK
- [ ] Runner attaché à votre projet OK
- [ ] Projet SonarQube OK
- [ ] Pipeline GitLab OK

## Etape 3 : Population de votre dépot

### Ajouter les fichiers suivants dans votre dépot

Average.php : Fichier contenant le code réalisé

Fichier src/but/Average.php :
```
<?php

namespace but;

class Average
{
    /**
     * Calculate the mean average
     * @param array $numbers Array of numbers
     * @return float Mean average
     */
    public function mean(array $Numbers)
    {
        $variable = "but";
        return array_sum($Numbers) / count($Numbers);
    }

    /**
     * Calculate the median average
     * @param array $numbers Array of numbers
     * @return float Median average
     */
    public function median(array $numbers)
    {
        sort($numbers);
        $size = count($numbers);
        if ($size % 2) {
            return $numbers[$size / 2];
        } else {
            return $this->mean(
                array_slice($numbers, ($size / 2) - 1, 2)
            );
        }
    }
}

```

### Verifier le statut de votre projet 

Vérifier le statut de votre pipeline CI
Vérifier le statut de votre projet dans SonarQube

### Correction de problèmes 

Corriger le fichier Average.php afin de corriger les problèmes identifié par SonarQube.

### Validation étape 3

A faire valider : 

- [ ] Statut de votre projet dans SonarQube
- [ ] Corrections apportées


## Etape 4 : Tests automatiques avec PHPUnit

### Ajouter les fichiers à votre dépots

boostrap.php : fichier permettant d'inclure les classes présentent dans le répertoire src
AverageTest.php : fichier contenant les tests unitaires des fonctions la classe Average.

Fichier tests/bootstrap.php
```
<?php

function loader($class): void
{
    $class_path = str_replace('\\', '/', $class);
    $file =  'src/' . $class_path . '.php';
    if (file_exists($file)) {
        require_once $file;
    }
}

spl_autoload_register('loader');
```

Fichier tests/but/AverageTest.php :
```
<?php

use but\Average;
use PHPUnit\Framework\TestCase;

class AverageTest extends TestCase
{
    protected $Average;

    public function setUp() : void
    {
        $this->Average = new Average();
    }

    public function testCalculationOfMean()
    {
        $numbers = [3, 7, 6, 1, 5];
        $this->assertEquals(4.4, $this->Average->mean($numbers));
    }

    public function testCalculationOfMedian()
    {
        $numbers = [3, 7, 6, 1, 5];
        $this->assertEquals(5, $this->Average->median($numbers));
    }
}
```

Les tests sont développés à l'aide d'un framework de test : [PHPUnit](https://phpunit.de/)

### Modifier le fichier de configuration SonarQube 

Afin que SonarQube intégre la structure du dépot modifié le fichier sonar-project.properties :

```
sonar.projectKey=<votre_project_key>
sonar.qualitygate.wait=true
sonar.tests=tests
sonar.sources=src
```

### Lancer les tests unitaires dans votre pipeline

Modifier le fichier gitlab-ci.yaml afin d'y intégrer le lancement des TU avant le lancement des tests SonarQube avec phpunit.
Pensez à vous aider de l'image docker [php:8.1](https://hub.docker.com/_/php) et de cet exemple [PHPunit/Gitlab CI](https://docs.gitlab.com/ee/ci/examples/php.html).

Afin que vos tests de coverage soient réalisés correctement vous devrez :
  - installer xdebug 3.3 à l'aide de PECL 
  - setter la variable XDEBUG_MODE à la valeur coverage  
dans votre job tu avant de lancer vos tests avec phpunit.

Installation de xdebug dans votre image gérant le job tu : 
```
pear config-set http_proxy http://<user>:<passowrd>@<proxy>:<port>
pecl install xdebug-3.3.0
docker-php-ext-enable xdebug 
```

Fichier de configuration de phpunit :
```
<?xml version="1.0" encoding="UTF-8"?>
<phpunit xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" bootstrap="tests/bootstrap.php" colors="true" xsi:noNamespaceSchemaLocation="https://schema.phpunit.de/10.5/phpunit.xsd">
    <testsuites>
        <testsuite name="Project Test Suite">
            <directory>tests</directory>
        </testsuite>
    </testsuites>
    <source>
        <include>
            <directory>src</directory>
        </include>
        <exclude>
            <directory>tests</directory>
        </exclude>
    </source>
</phpunit>
```

Vérifier que votre pipeline execute corretement les tests PHPUnit.

### Génération des rapports de test

Faire en sorte que PHPunit génère 2 rapports :
  - rapport de test (format junit) : build/rapport-test.xml
  - rapport de coverage (format clover) :  build/rapport-coverage.xml


### Upload des artefacts 


Définir les rapports de test en temps qu'artefact et créer une dépendance entre votre job TU et sonarqube afin que les artefacts puissent être récupérés par SonarQube.

### Intégration des rapport à SonarQube

Intégrer vos rapports de coverage et de tests à SonarQube en modifiant le fichier sonar-project.properties


### Correction de vos fichier

Corriger vos fichiers afin que la quality gate SonarQube soit OK.

### Validation étape 4

A faire valider : 

- [ ] Rapport de tests
- [ ] Rapport de coverage
- [ ] Présence de vos rapports dans les artefacts Gitlab CI
- [ ] Dépendance entre vos jobs TU et SonarQube
- [ ] Résumer des tests présent dans le statut de votre pipeline Gitlab CI
- [ ] Prise en compte de vos rapports de tests et de coverages par SonarQube

## Etape 5 : Quality Gate SonarQube

### Corrections de vos sources

Corriger vos sources afin de n'avoir plus aucun problème de type "code Smell"

### Corrections de la couverture de test

Modifier les tests pour obtenir 100% de couverture du code à 100%
Faire en sorte que le/les tests ajoutés ne soit pas OK.
Faire en sorte que le job TU finisse sans erreur avec ou sans présence d'erreurs (code retour 0).

### Modification Quality Gate SonarQube

Bien que l'ensemble des tests ne soient pas OK, le Quality Gate SonarQube est OK.
Créer une quality gate but3 identique à celle par défaut qui inclus en plus une contrainte avec 100% des tests OK.
Configurer votre projet avec cette Quality Gate

### Validation étape 5

A faire valider : 

- [ ] job TU non bloquant en cas d'erreur
- [ ] Quality Gate but3
- [ ] Statut de votre pipeline GitLab & projet SonarQube avec un TU KO, puis tous les TU OK.

## Etape 6 : Déploiement Continue

### Créer une application 

Créer une application de votre choix qui utilisera la librairie but\Average.
L'application est située dans le fichier src/index.php.

### Créer un job "release"

Ce job de releasing devra : 
 - être dépendant du passage de la quality gate.
 - créer une image docker "but-$groupe" qui embarquera votre application
 - déposer cette image docker dans la registry docker de votre projet gitlab

Aidez vous de la [documentation Gitlab](https://docs.gitlab.com/ee/user/packages/container_registry/build_and_push_images.html)

Fichier Dockfile nécessaire à la construction de votre image docker à la racine de votre dépôt :
```
FROM php:8.1-apache
COPY src/ /var/www/html/

```

### Crer un job "deploy"

Le job de déploiement consiste à se connecter en SSH sur la machine cible du déploiement (SSH_HOST) à l' aide d'un nom d'utilisateur (SSH_USER) et d'une clé privée (SSH_KEY).
Puis de lancer sur la machine cible une instance de l'image docker générée lors du job "release".

Ce job de déploiement devra : 
 - être dépendant du releasing de votre image docker
 - se baser sur une image [ubuntu:latest](https://hub.docker.com/_/ubuntu)
 - s'appuyer sur des variables d'environnements de votre projet définit ci dessous


 Variables d'environnements à configurer :
 - SSH_HOST : 172.17.x.x
 - SSH_USER : but
 - SSH_KEY :
```
TbC
```
 - DOCKER_NAME : but-run-$groupe
 - DOCKER_PORT : 10 000 + $groupe (10 006 si $groupe est égal à 6)

### Validation étape 6

A faire valider : 

- [ ] pipeline intégrant 4 jobs : TU, SonarQube, release, deploy
- [ ] Image docker présente dans la registry
- [ ] Variables d'environnement correctements renseignées dans le projet
- [ ] Lancer une modification du index.php, suivre la pipeline et visualiser la modication sur http://SSH_HOST:DOCKER_PORT/
